import {Router } from "express";
import { Articlerepository } from "../Repository/Article-repostiroy";
import passport from "passport";





export const articlecontroler=Router();

articlecontroler.get('/all', async (req, res)=>{
    let art=await Articlerepository.findAll();
    res.json(art)
})

articlecontroler.post('/',passport.authenticate('jwt', {session:false}),async (req,res) => {
     await Articlerepository.add(req.body);
    res.status(201).json(req.body)
})

articlecontroler.put('/:id',async(req,res)=>{
    await Articlerepository.update(req.body);
    res.end()
})

articlecontroler.delete('/:id',async(req,res)=>{
    await Articlerepository.delete(req.params.id);
    res.end()
})

articlecontroler.get('/id/:id',async (req,res)=>{
    let art=await Articlerepository.findblogByid(req.params.id)
    if (!art){
        response.status(404).json({error:'Not found'});
        return;
        
    }
    res.json(art)
})

articlecontroler.get('/month/:date', async(req, res)=>{
    let art = await Articlerepository.findByDate(req.params.date)
    res.json(art)
}
)
