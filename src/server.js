import express from 'express';
import cors from 'cors';
import { articlecontroler } from './controller/article-controller';
import { userController } from './controller/user-controller';
import { configurePassport } from './utils/token';
configurePassport()
export const server = express();

server.use(express.json());
server.use(cors());
server.use('/api/blog/article',articlecontroler)
server.use('/api/blog/user', userController)