import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Userrepository } from '../Repository/User-repository';



/* Fonction qui génère un JWT en se basant sur la clef privée.
 * Il est actuellement configuré pour expiré au bout de 1H (60*60 secondes) mais ça peut être modifié
 * @param {object} payload le body qui sera mis dans le token
 * @returns le JWT
 */
export function generateToken(payload, expire = 6060) {
    const token = jwt.sign(payload, process.env.JWT_SECRET,{expiresIn: expire });
    return token;
}

/*
 * Fonction qui configure la stratégie JWT de passport, il va chercher le token dans les headers de la 
 * requête en mode {"authorization":"Bearer leToken"}
 */
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, async (payload, done) => {

        // method findByEmail to retrieve the User and be able to give access to it in protected routes
        try {
            const user = await Userrepository.findByEmail(payload.email);

            if (user) {

                return done(null, user);
            }

            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}

/**
 * Function to protect a route
 * @param {string[]} role an array of roles allowed to access the route (if any, then any role can access it)
 * @returns  array of middleware jwt
 */
export function protect(role = ['any']) {

    return [
        passport.authenticate('jwt', { session: false }),
        (req, res, next) => {
            if (role.includes('any') || role.includes(req.user.role)) {
                next()
            } else {
                res.status(403).json({ error: 'Access denied' });
            }
        }
    ]
}